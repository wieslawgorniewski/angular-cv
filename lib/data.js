window.RESUME_DATA = {
  "personal_info": {
    "color": "Blue",
    "submenus": {
      "personal": {
        "name": "Wieslaw",
        "surname": "Gorniewski",
        "title": "Software Engineer"
      },
      "contact_data": {
        "email": "wieslaw.gorniewski@xxxxx.xx",
        "phone": "+1 (669) xxx xxx",
        "skype": "wieslaw.gorniewski",
        "location": "Sunnyvale, CA, USA",
        "work_authorization": "Green Card"
      }
    }
  },
  "experience": {
    "color": "Orange",
    "submenus": {
      "experience": [
        {
          "entry": {
            "date_start": "2016/08",
            "date_end": "present",
            "title": "Software Engineer",
            "name": "drchrono",
            "description": [
              "Back-end development with Django framework",
              "Front-end development with HTML5, CSS3, and Javascript / AngularJS",
              "Unit and integration testing in Django Test Framework",
              "Working in Agile/Scrum methodology"
            ]
          }
        },
        {
          "entry": {
            "date_start": "2015/11",
            "date_end": "2016/07",
            "title": "Software Engineer",
            "name": "Polcode",
            "description": [
              "Back-end development with Ruby On Rails framework",
              "Front-end development with HTML5, CSS3, and Javascript / jQuery",
              "Unit testing in RSpec, behavioral testing using Cucumber",
              "Working in Agile/Scrum methodology"
            ]
          }
        },
        {
          "entry": {
            "date_start": "2013/11",
            "date_end": "2015/11",
            "title": "Software Engineer",
            "name": "Mobica",
            "description": [
              "Creating Web Applications and RESTful services",
              "Website design, User Experience design, database/software design",
              "Programming in Python, Ruby, Javascript languages",
              "Back-end development with Python/Django, Ruby/Ruby On Rails MVC frameworks",
              "Front-end development with HTML5, CSS3, and Javascript, mainly jQuery, jQueryUI",
              "Unit (Django Test Framework, RSpec), behavioral (Behave, Cucumber) testing",
              "Work in several on-site projects in England",
              "Project 1 - Social App Clients made for SmartTVs in MAF Framework (Javascript)",
              "Project 2 - backed RESTful API made in Django/Django Piston (later Django Rest Framework)",
              "Project 3 - Web Application for calculating taxes made in Ruby On Rails 3",
              "Project 4 - eCommerce Web Application made in Django/Django Oscar"
            ]
          }
        },
        {
          "entry": {
            "date_start": "2012/10",
            "date_end": "2015/06",
            "title": "Freelancer",
            "description": [
              "Idea, project and realization both front-end and back-end of the Website",
              "Django (Python), HTML, CSS, jQuery, AngularJS, Ajax (Javascript)"
            ]
          }
        }
      ],
      "education": [
        {
          "entry": {
            "date_start": "2004",
            "date_end": "2009",
            "title": "Computer Science",
            "name": "Academy of Humanities and Economics in Lodz"
          }
        },
        {
          "entry": {
            "date_start": "1998",
            "date_end": "2004",
            "title": "Specialization in Graphics",
            "name": "Secondary School of Fine Arts in Bydgoszcz"
          }
        }
      ]
    }
  },
  "skills": {
    "color": "Green",
    "submenus": {
      "professional_skills": [
        {
          "entry": {
            "name": "Python",
            "level": 4
          }
        },
        {
          "entry": {
            "name": "Django",
            "level": 4
          }
        },
        {
          "entry": {
            "name": "RSpec",
            "level": 4
          }
        },
        {
          "entry": {
            "name": "Cucumber",
            "level": 4
          }
        },
        {
          "entry": {
            "name": "REST",
            "level": 4
          }
        },
        {
          "entry": {
            "name": "HTML5",
            "level": 4
          }
        },
        {
          "entry": {
            "name": "CSS3",
            "level": 4
          }
        },
        {
          "entry": {
            "name": "Git",
            "level": 4
          }
        },
        {
          "entry": {
            "name": "Ruby On Rails",
            "level": 4
          }
        },
        {
          "entry": {
            "name": "Javascript",
            "level": 3
          }
        },
        {
          "entry": {
            "name": "jQuery",
            "level": 3
          }
        },
        {
          "entry": {
            "name": "AngularJS",
            "level": 3
          }
        },
        {
          "entry": {
            "name": "React",
            "level": 3
          }
        },
        {
          "entry": {
            "name": "Ruby",
            "level": 3
          }
        },
        {
          "entry": {
            "name": "Unity3D",
            "level": 2
          }
        },
        {
          "entry": {
            "name": "Java",
            "level": 2
          }
        }
      ],
      "languages": [
        {
          "entry": {
            "name": "Polish",
            "level": 5
          }
        },
        {
          "entry": {
            "name": "English",
            "level": 4
          }
        }
      ],
      "other": [
        {
          "entry": {
            "name": "Quality Oriented",
            "level": 5
          }
        },
        {
          "entry": {
            "name": "Fast learner",
            "level": 5
          }
        },
        {
          "entry": {
            "name": "Cheerful",
            "level": 5
          }
        },
        {
          "entry": {
            "name": "Team player",
            "level": 5
          }
        },
        {
          "entry": {
            "name": "Resourceful",
            "level": 5
          }
        }
      ]
    }
  }
}
