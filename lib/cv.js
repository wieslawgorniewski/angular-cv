var container = document.getElementById('contentMain');
Ps.initialize(
    container,
    {
        theme: 'my-theme-name',
        suppressScrollX: true
    }
);
var resume = angular.module('resume', ['ngAnimate']);

resume.controller('ResumeController', function ($scope, sharedData) {
    $scope.types = ['print', 'presentation'];
    $scope.sharedData = sharedData;
    $scope.resumeData = window.RESUME_DATA;

    $scope.setResumeData = function($fileContent){
        $scope.resumeData = YAML.parse($fileContent);
    }

    $scope.selectScreen = function(element) {
        container.scrollTop = 0;
        Ps.update(container);
        $scope.sharedData.menuSelect(element);
        $scope.selectedScreen = element;
    }

    $scope.init = function () {
        sharedData.menusCreate($scope.resumeData);
        $scope.selectScreen(sharedData.menu[0]);
    }

    $scope.init();
});

resume.factory('sharedData', function() {
    var shared = {};

    shared.getRange = function(number) {
        return new Array(number);
    }

    shared.menu = [];

    shared.menuSelect = function(element) {
        angular.forEach(shared.menu, function(value) {
            if (value.name == element.name) {
              value.selected = true;
            } else {
              value.selected = false;
            }
        });
    }

    shared.menusCreate = function(data) {
         angular.forEach(data, function(values, name) {
            shared.menu.push(
                {
                    name: name.replace(/_/g, ' ').toUpperCase(),
                    id: name,
                    color: 'bg' + values.color,
                    background: 'gr' + values.color,
                    selected: false

                }
            );
            shared.menu[0].selected = true;
        });
    }

    shared.camelize = function(str) {
        return str.toLowerCase().replace(/(?:^\w|[A-Z]|\b\w)/g, function(letter, index) {
            return index == 0 ? letter.toLowerCase() : letter.toUpperCase();
        }).replace(/\s+/g, '');
    }

    return shared;
});

resume.directive('skillCreate', function ($parse, $timeout) {
    return {
        restrict: 'A',
        scope: false,
        replace: false,
        transclude: true,
        link: function(scope, element, attrs) {
            $timeout(function () {
                Circles.create({
                    id:                  attrs.id,
                    radius:              20,
                    value:               attrs.level * 20,
                    maxValue:            100,
                    width:               4,
                    text:                attrs.name,
                    colors:              ['transparent', '#fff'],
                    duration:            3,
                    wrpClass:            'circles-wrp',
                    textClass:           'skillText',
                    valueStrokeClass:    'circles-valueStroke',
                    maxValueStrokeClass: 'circles-maxValueStroke',
                    styleWrapper:        true,
                    styleText:           true
                });
            });
        }
    };
});

resume.filter('isEmpty', function () {
    var element;
    return function (object) {
        for (element in object) {
            if (object.hasOwnProperty(element)) {
                return false;
            }
        }
        return true;
    };
});

resume.filter('isArray', function () {
    return function (object) {
        return object.constructor === Array;
    };
});

resume.filter('removeEntryLevel', function () {
    var obj = [];
    return function (array) {
        if (array.constructor !== Array) {
            return;
        }
        for (var i = 0; i < array.length; i++) {
            if (array[i].entry) {
                obj.push(array[i].entry);
            }
        }
        return '';
    };
});

resume.filter('rangeCreate', function () {
    return function (num) {
        return new Array(num);
    };
})

resume.filter("regexReplace", function() {
    return function(input, searchRegex, replaceRegex) {
        return input.replace(RegExp(searchRegex, 'g'), replaceRegex);
    };
});
